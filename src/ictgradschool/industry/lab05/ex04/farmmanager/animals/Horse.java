package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by qpen546 on 20/03/2017.
 */
public class Horse extends Animal{
    private static final String name = "Horse";

    private int value;

    public Horse() {
        value = 1500;
    }

    public void feed() {
        if (value < 3000) {
            value += (3000-value)/2;
        }
    }

    public int costToFeed(){
        return 100;
    }

    public String getName(){
        return name;
    }

    public int getValue(){
        return value;
    }

    public String toString(){
        return name+" - $"+value;
    }

}
