package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by qpen546 on 20/03/2017.
 */
public class Sheep extends Animal implements ProductionAnimals{
    private static final String name = "Sheep";

    private int value;

    public Sheep() {
        value = 800;
    }

    public void feed() {
        if (value < 1200) {
            value += (1200-value)/2;
        }
    }

    public int costToFeed(){
        return 40;
    }

    public String getName(){
        return name;
    }

    public int getValue(){
        return value;
    }

    public String toString(){
        return name+" - $"+value;
    }

    @Override
    public boolean harvestable(){
        return tick >= 5;
    }

    @Override
    public int harvest(){
        if (harvestable()){
            tick = 0;
            return 40;
        }else{
            return 0;
        }
    }
}
