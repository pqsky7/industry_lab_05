package ictgradschool.industry.lab05.ex04.farmmanager.animals;

public abstract class Animal {
	public int tick= 0;
	public abstract int costToFeed();
	public abstract void feed();

	public abstract String getName();

	public abstract int getValue();

	public abstract String toString();

	public void tick(){
		tick ++;
		System.out.println(getName()+" have tick:"+tick + " ");
	}

}
