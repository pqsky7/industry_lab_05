package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by qpen546 on 20/03/2017.
 */
public interface ProductionAnimals {
    public boolean harvestable();
    public int harvest();
}
