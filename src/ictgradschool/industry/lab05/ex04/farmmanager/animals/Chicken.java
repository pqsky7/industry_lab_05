package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by qpen546 on 20/03/2017.
 */
public class Chicken extends Animal implements ProductionAnimals{
    private static final String name = "Chicken";

    private int value;

    public Chicken() {
        value = 100;
    }

    public void feed() {
        if (value < 200) {
            value += (200-value)/3;
        }
    }

    public int costToFeed(){
        return 1;
    }

    public String getName(){
        return name;
    }

    public int getValue(){
        return value;
    }

    public String toString(){
        return name+" - $"+value;
    }

    @Override
    public boolean harvestable(){
        return tick >= 3;
    }

    @Override
    public int harvest(){
        if (harvestable()){
            tick = 0;
            return 3;
        }else{
            return 0;
        }
    }
}
